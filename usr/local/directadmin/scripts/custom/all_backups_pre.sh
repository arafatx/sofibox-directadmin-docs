#/bin/sh - adminbckp-pre (for all_backups_pre.sh)
# Script of admin_backups to run before DA Admin Backup is trigger
function adminbckp_clean()
{
#GLOBAL VARS
MYEMAIL="webmaster@sofibox.com"
USER_NAME=admin #directadmin username. which user should view this backup file in file manager. example admin
ADMIN_BACKUP_TEMP_PATH=/backup/da/temp/admin_backups
ADMIN_BACKUP_NEW_PATH=/home/$USER_NAME/admin_backups
REPORT_FILE="/tmp/adminbckp-script.log"
MYHOSTNAME=`/bin/hostname`
MAIL_BIN="/usr/local/bin/mail"
WARNING_STATUS="OK"
# Clear the log file first
cat /dev/null > $REPORT_FILE
echo "======================================================================================" | tee -a  $REPORT_FILE
echo "[adminbckp-pre][info] System Backup Pre Script checked on `date`" | tee -a $REPORT_FILE
# Move current backup to temporary folder if the backup exists
if [ "$(ls -A $ADMIN_BACKUP_NEW_PATH)" ]; then
   echo "[adminbckp-pre][notice] There are one or more backup exist in $ADMIN_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
   echo "[adminbckp-pre][notice] Moving existing backup files into a temporary folder in $ADMIN_BACKUP_TEMP_PATH ..." | tee -a $REPORT_FILE
   mv "${ADMIN_BACKUP_NEW_PATH}"/* "${ADMIN_BACKUP_TEMP_PATH}"/
   echo "[adminbckp-pre][done] Existing backup files have been transferred into a temporary folder at $ADMIN_BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
else
   echo "[adminbckp-pre][info] No existing backup is found in $ADMIN_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
   echo "======================================================================================" | tee -a  $REPORT_FILE
   echo "[adminbckp][info] Running Directadmin Admin Backup. Please wait ..." | tee -a $REPORT_FILE
fi
# Exit script
exit 0
}

##############
#Function call:
adminbckp_clean
##############