#!/bin/sh
# This script is run after the DA System Backup is triggered
# Author: Arafat Ali (arafat@sofibox.com)

#GLOBAL VARS
MYEMAIL="webmaster@sofibox.com"
USER_NAME=admin

ADMIN_BACKUP_TEMP_PATH=/backup/da/temp/admin_backups
ADMIN_BACKUP_ORI_PATH=/backup/da/admin_backups
ADMIN_BACKUP_NEW_PATH=/home/$USER_NAME/admin_backups

MAIL_BIN="/usr/bin/mailx"
WARNING_STATUS="OK"
MYHOSTNAME=`/bin/hostname`

REPORT_FILE="/tmp/adminbckp-script.log"

BACKUP_DATE_NOW="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020_11-56-16
BACKUP_FILE_NAME_NOW="admin_backups-$BACKUP_DATE_NOW.tar.gz" #admin_backups-31-03-2020--11-56-16.tar.gz

BACKUP_DIRS=(/backup/da/admin_backups/*) #MULTIPLE array
#BACKUP_DIR_TO_ARCHIVE="${BACKUP_DIRS[0]}" #SINGLE /backup/da/admin_backup/admin_backups-31-03-2020_11-56-16

#BACKUP_NEW_FULL_PATH="$ADMIN_BACKUP_NEW_PATH/$BACKUP_FILE_NAME_NOW"
#admin_backups-%d-%h-%Y_%H-%M-%S <--- format folder backup name from DA

echo "======================================================================================" | tee -a  $REPORT_FILE
if [ "${success}" == "1" ]; then
        echo "[adminbckp-post][ok] Backup reported success by Directadmin-Admin Backup Server" | tee -a $REPORT_FILE
        # Check if the backup directory at /backup/da/admin_backups is empty or not for any potential reason
        if [ ! -d "$BACKUP_DIRS" ]; then
                WARNING_STATUS="WARNING"
                echo "[adminbckp-post][error] Backup directory is empty or does not exist in $ADMIN_BACKUP_ORI_PATH to be archived" | tee -a $REPORT_FILE
        else
                echo "[adminbckp-post][notice] There is one or more backup directory exist in $ADMIN_BACKUP_ORI_PATH to be archived" | tee -a $REPORT_FILE
                # Archive every single folder
                for da_backup_dir in "${BACKUP_DIRS[@]}"
                do
                        echo "[adminbckp-post][notice] Compressing backup folder of $da_backup_dir as tar.gz ..." | tee -a $REPORT_FILE
                        tar -c --use-compress-program=pigz -f "$da_backup_dir.tar.gz" "$da_backup_dir" | tee -a $REPORT_FILE
                        echo "[adminbckp-post][done] Backup folder of $da_backup_dir has been sucessfully compressed" | tee -a $REPORT_FILE
                        ################
                        echo "[adminbckp-post][notice] Validating backup file $da_backup_dir.tar.gz ..." | tee -a $REPORT_FILE
                        if pigz -cvd "$da_backup_dir.tar.gz" | tar -tv > /dev/null; then
                                echo "[adminbckp-post][ok] Backup file validation passed for $da_backup_dir.tar.gz" | tee -a $REPORT_FILE
                                rm -rf $da_backup_dir
                                echo "[adminbckp-post][ok] Backup folder of $da_backup_dir has been deleted" | tee -a $REPORT_FILE
                                #Change the file permissions of the complete files which located at the New full path
                                chown -R $USER_NAME:root $da_backup_dir.tar.gz
                                chmod -R 644 $da_backup_dir.tar.gz
                                echo "[adminbckp-post][done] Backup file permission modified to current user at $ADMIN_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
                                #Transfer semua files yang telah compress di root backup ke dalam path baru untuk view kat file manager
                                #mv "${ADMIN_BACKUP_ORI_PATH}"/* "${ADMIN_BACKUP_NEW_PATH}"/
                                #echo "[adminbckp-post][done] Backup files from $ADMIN_BACKUP_ORI_PATH have been transferred into $ADMIN_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
                                echo "[adminbckp-post][notice] Uploading archived backup file $da_backup_dir.gz into onedrive folder ..." | tee -a $REPORT_FILE
                                rclone move "$da_backup_dir.tar.gz" microsoftonedrive:Backup/Server/earth.sofibox.com/admin_backups/ | tee -a $REPORT_FILE
                                echo "[adminbckp-post][done][Backup file $da_backup_dir.tar.gz has moved to onedrive" | tee -a $REPORT_FILE
                        else
                                echo "[adminbckp-post][error] File validation is not passed! Please inspect the backup archived file" |tee -a $REPORT_FILE
                                WARNING_STATUS="WARNING"
                        fi
                        ##############
                done
        fi # END CHECK DIRECTORY BACKUP ORI
else
        # For backup failed reported from DA
        WARNING_STATUS="WARNING"
        echo "[adminbckp-post][$WARNING_STATUS] Backup reported error, please check the log files" >> $REPORT_FILE
fi

if find "$ADMIN_BACKUP_TEMP_PATH" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
        echo "[adminbckp-post][notice] Existing backup files found at $ADMIN_BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
        echo "[adminbckp-post][notice] Transferring existing backup to $ADMIN_BACKUP_NEW_PATH ..." | tee -a $REPORT_FILE
        mv "${ADMIN_BACKUP_TEMP_PATH}"/* "${ADMIN_BACKUP_NEW_PATH}"/
        echo "[adminbckp-post][done] Existing backup files have been transferred into $ADMIN_BACKUP_NEW_PATH successfully" | tee -a $REPORT_FILE
fi
echo "[adminbckp-post][done] System Admin Post Script has finished running" | tee -a $REPORT_FILE
echo "======================================================================================" | tee -a  $REPORT_FILE
$MAIL_BIN  -s "[adminbckp-post][$WARNING_STATUS] Admin Backup Post Script Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
#rm -f $REPORT_FILE
exit 0