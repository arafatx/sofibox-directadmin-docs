#! /bin/sh
#aide-check.sh - EARTH.SOFIBOX.COM

#Global Vars
script_name=$(basename -- "$0")
pid=(`pgrep -f $script_name`)
pid_count=${#pid[@]}
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
DATE_BIN=`/usr/bin/date +%s`
AIDE_BIN="/usr/sbin/aide"
MAIL_BIN="/usr/local/bin/mail"
RM_BIN="/bin/rm"
MV_BIN="/bin/mv"
AIDE_CONFIG="/usr/local/bin/maxicron/aide/aide.conf"
AIDE_DB_PATH="/usr/local/bin/maxicron/aide/db"
AIDE_LOG_PATH="/usr/local/bin/maxicron/aide/log"
WARNING_STATUS="N/A"
REPORT_FILE="$AIDE_LOG_PATH/aide-cron-log.$DATE_BIN.$RANDOM.log"
AIDE_DB_FILE="$AIDE_DB_PATH/aide.db.gz"
AIDE_DB_FILE_OUT="$AIDE_DB_PATH/aide.db.new.gz"
#AIDE_LOG_FILE=
RUNNING_ARGS="$1" #auto=cron

if [ -z "$1" ]; then
        RUNNING_ARGS="manual"
fi
# TODO #DELETE_ENTRY=0 #ADD_ENTRY=0 #CHANGE_ENTRY=0 # IMPORTANT TODO: DELETE LOG FILES WITHIN SPECIFIC DAY #LOG_DAYS_TO_KEEP=0
echo "===============================" > $REPORT_FILE
echo "[1][aide] AIDE checked on `date`" >> $REPORT_FILE
echo "===============================" >> $REPORT_FILE
echo "[aide] Running mode: $RUNNING_ARGS" >> $REPORT_FILE
echo "" >> $REPORT_FILE
[[ -z $pid ]] && echo "Failed to get the PID"

if [ -f "/var/run/$script_name" ];then
        if [[  $pid_count -gt "1" ]];then
                echo "=================================" | tee -a $REPORT_FILE
                echo "[error] another instance of $script_name script is already running, clear all the sessions of $script_name to initialize session" | tee -a $REPORT_FILE
                echo "[error] for this reason, $script_name script is terminated" | tee -a $REPORT_FILE
                echo "===============================" | tee -a $REPORT_FILE
                $MAIL_BIN -s "[aide][error][$RUNNING_ARGS] AIDE File Integrity Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
                echo "[aide] Email is set to be sent to $MYEMAIL"
                echo "==============================="
                exit 1
        else
                echo "=================================" | tee -a $REPORT_FILE
                echo "[error] the last instance of $script_name script exited unsuccessfully, performing cleanup..." | tee -a $REPORT_FILE
                rm -f "/var/run/$script_name"
                echo "[done] an instance of $script_name script has been removed successfully from the lock" | tee -a $REPORT_FILE
                echo "=================================" | tee -a $REPORT_FILE
        fi
fi

echo $pid > /var/run/$script_name
echo "================================="
echo "[aide] AIDE is checking system..."
echo "[aide] Running mode: $RUNNING_ARGS"
echo "[aide] Please wait..."
$AIDE_BIN --check --config="$AIDE_CONFIG" >> $REPORT_FILE
echo "" >> $REPORT_FILE

echo "===============================" >> $REPORT_FILE
echo "[2][aide] Initializing AIDE new database..." >> $REPORT_FILE
echo "===============================" >> $REPORT_FILE
$AIDE_BIN --init --config="$AIDE_CONFIG" >> $REPORT_FILE
echo "[OK] AIDE DB initialized successfully" >> $REPORT_FILE
echo "" >> $REPORT_FILE

echo "===============================" >> $REPORT_FILE
echo "[3][aide] Checking existing AIDE DB..." >> $REPORT_FILE
echo "===============================" >> $REPORT_FILE

if [[ ! -f $AIDE_DB_FILE_OUT ]] ; then
        echo "[WARNING] No existing AIDE DB Found" >> $REPORT_FILE
else
        echo "[OK] Existing AIDE DB found. Updating database ..." >> $REPORT_FILE
        $MV_BIN $AIDE_DB_FILE_OUT $AIDE_DB_FILE
        echo "[OK] Done. Existing AIDE DB updated successfully" >> $REPORT_FILE
fi
echo "" >> $REPORT_FILE

echo "===============================" >> $REPORT_FILE
echo "[4][aide] Checking AIDE DB integrity after database initialization..." >> $REPORT_FILE
echo "===============================" >> $REPORT_FILE
$AIDE_BIN --update --config="$AIDE_CONFIG" >> $REPORT_FILE
echo "[OK] Done checking AIDE DB integrity" >> $REPORT_FILE
echo "" >> $REPORT_FILE

if (grep -e "found differences" -e "WARNING" $REPORT_FILE) then
        WARNING_STATUS="WARNING"
else
        WARNING_STATUS="OK"
fi

echo "================================" >> $REPORT_FILE
echo "[aide] Scan completed successfully" >> $REPORT_FILE
echo "[aide] Scan status: " $WARNING_STATUS >> $REPORT_FILE
echo "================================" >> $REPORT_FILE

if [ "${WARNING_STATUS}" == "WARNING" ]; then
        $MAIL_BIN -s "[aide][$WARNING_STATUS|$RUNNING_ARGS|N] AIDE File Integrity Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
fi

$RM_BIN -f $REPORT_FILE
$RM_BIN -f "/var/run/$script_name"

echo "[aide] Scan status: $WARNING_STATUS"
echo "[aide] Done checking system. Email is set to be sent to $MYEMAIL"
echo "==============================="