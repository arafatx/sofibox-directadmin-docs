#/bin/sh - system_backups_pre.sh
# Script of system_backups to run before DA System Backup is trigger

function systembckp_clean()
{
        #GLOBAL VARS
        MYEMAIL="webmaster@sofibox.com"
        USER_NAME=admin #directadmin username. which user should view this backup file in file manager. example admin
        SYSTEM_BACKUP_TEMP_PATH=/backup/da/temp/system_backups
        SYSTEM_BACKUP_NEW_PATH=/home/$USER_NAME/system_backups
        SYSBCKP_LOG_PATH="/usr/local/bin/maxicron/da-backups/log"
        REPORT_FILE="$SYSBCKP_LOG_PATH/systembckp-script.log"
        MYHOSTNAME=`/bin/hostname`
        MAIL_BIN="/usr/local/bin/mail"
        WARNING_STATUS="OK"

        echo "===================================================================" | tee $REPORT_FILE
        echo "[systembckp-pre][info] System Backup Pre Script checked on `date`" | tee -a $REPORT_FILE
        echo "[systembckp-pre][info] Cleaning packages and temporary files through custombuild clean command ..." | tee -a $REPORT_FILE
        echo "[systembckp-pre][info] Please wait..." | tee -a $REPORT_FILE
        #/usr/local/directadmin/custombuild/./build update | tee -a $REPORT_FILE
        /usr/local/directadmin/custombuild/./build clean | tee -a $REPORT_FILE
        /usr/local/directadmin/custombuild/./build clean_old_webapps | tee -a  $REPORT_FILE
        /usr/local/directadmin/custombuild/./build clean_old_tarballs | tee -a $REPORT_FILE
        echo "[systembckp-pre][done] Custombuild clean command executed successfully " | tee -a $REPORT_FILE
        # Manual file remove
        echo "[systembckp-pre][info] Removing DA system packages with rm command (eg: *.tgz, *.tar.gz, *.tar.bz2) ..." | tee -a $REPORT_FILE
        rm -f /usr/local/directadmin/custombuild/*.tgz
        rm -f /usr/local/directadmin/custombuild/*.tar.gz
        rm -f /usr/local/directadmin/custombuild/*.tar.bz2
        rm -rf /usr/local/directadmin/custombuild/mysql
        rm -rf /usr/local/directadmin/scripts/packages/*
        rm -rf /usr/local/directadmin/custombuild/mysql_backups/*
        echo "[systembckp-pre][done] DA system packages have been removed successfully with rm command" | tee -a $REPORT_FILE
        # Move current backup to temporary folder if the backup exists
        if [ "$(ls -A $SYSTEM_BACKUP_NEW_PATH)" ]; then
                echo "[systembckp-pre][notice] There are one or more backup exist in $SYSTEM_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
                echo "[systembckp-pre][notice] Moving backup files into temporary folder in $SYSTEM_BACKUP_TEMP_PATH ..." | tee -a $REPORT_FILE
                mv "${SYSTEM_BACKUP_NEW_PATH}"/* "${SYSTEM_BACKUP_TEMP_PATH}"/
                echo "[systembckp-pre][done] Existing backup files have been transferred into a temporary folder at $SYSTEM_BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
        else
                echo "[systembckp-pre][info] No existing backup is available in $SYSTEM_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
        fi
        echo "[systembckp-pre][info] System Backup Pre Script has finished running" | tee -a $REPORT_FILE
        echo "===================================================================" | tee -a $REPORT_FILE
        echo "[systembckp-pre][info] Running Directadmin System Backup. Please wait ..." | tee -a $REPORT_FILE
        # Exit script
        exit 0
}

##############
#Function call:
systembckp_clean
#############