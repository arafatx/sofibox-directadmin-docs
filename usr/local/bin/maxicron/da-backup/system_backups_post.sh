#!/bin/sh
# This script is run after the DA System Backup is triggered
# Author - Arafat Ali
# Kerja Perintah Berkurung #Covid19 #StayHome#

#GLOBAL VARS
MYEMAIL="webmaster@sofibox.com"
USER_NAME=admin #directadmin username. which user should view this backup file in file manager. example admin
BACKUP_SECURITY_CODE="12345"

SYSTEM_BACKUP_TEMP_PATH=/backup/da/temp/system_backups
SYSTEM_BACKUP_ORI_PATH=/backup/da/system_backups
SYSTEM_BACKUP_NEW_PATH=/home/$USER_NAME/system_backups

MAIL_BIN="/usr/bin/mailx"
WARNING_STATUS="OK"
MYHOSTNAME=`/bin/hostname`

SYSBCKP_LOG_PATH="/usr/local/bin/maxicron/da-backups/log"
REPORT_FILE="$SYSBCKP_LOG_PATH/systembckp-script.log"

BACKUP_DATE_NOW="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020--11-56-16
BACKUP_TIME_NOW="$(date '+_%H-%M-%S')" #11-56-16
BACKUP_FOLDER_NAME_NOW="system_backups-$BACKUP_DATE_NOW"
BACKUP_FILE_NAME_NOW="system_backups-$BACKUP_DATE_NOW.tar.gz" #system_backups-31-03-2020--11-56-16-AM.tar.gz

BACKUP_DIRS=(/backup/da/system_backups/*) #MULTIPLE array
#BACKUP_DIR_TO_ARCHIVE="${BACKUP_DIRS[0]}" #SINGLE /backup/da/system_backup/31-03-2020

#BACKUP_ORI_FULL_PATH="$BACKUP_ORI_PATH/$BACKUP_FILE_NAME_NOW"
#BACKUP_NEW_FULL_PATH="$BACKUP_NEW_PATH/$BACKUP_FILE_NAME_NOW"

RUNNING_ARGS="$1" #pass-backup-code

#TODO #BACKUP_TYPE = zip / tar.gz / rar
echo "================================================================" | tee -a  $REPORT_FILE
## START CHECKING RUNNING DUPLICATED PROCESS ##
script_name=$(basename -- "$0")
pid=(`pgrep -f $script_name`)
pid_count=${#pid[@]}
[[ -z $pid ]] && echo "[systembckp-post][error] Failed to get the PID" | tee -a $REPORT_FILE

if [ -f "/var/run/$script_name" ];then
        if [[  $pid_count -gt "1" ]];then
                WARNING_STATUS="WARNING"
                echo "=========================================================" | tee -a $REPORT_FILE
                echo "[systembckp-post][error] An another instance of $script_name script is already running, clear all the sessions of $script_name to initialize session" | tee -a $REPORT_FILE
                echo "[systembckp-post][error] The $script_name script is terminated for this reason" | tee -a $REPORT_FILE
                $MAIL_BIN -s "[systembckp-post][$WARNING_STATUS] System Backup Post Script Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
                echo "[sysbckp-post][done] Email is set to be sent to $MYEMAIL"
                echo "=========================================================" | tee -a $REPORT_FILE
                #Remove log and lock status
                rm -f $REPORT_FILE
                rm -f "/var/run/$script_name"
                # terminate script
                exit 1
        else
                echo "=================================" | tee -a $REPORT_FILE
                echo "[sysbckp-post][warning] The last instance of $script_name script exited unsuccessfully, performing cleanup..." | tee -a $REPORT_FILE
                rm -f "/var/run/$script_name"
                echo "[sysbckp-post][done] An instance of $script_name script has been removed successfully from the lock" | tee -a $REPORT_FILE
                echo "=================================" | tee -a $REPORT_FILE
        fi
fi
## END CHECKING RUNNING DUPLICATED PROCESS ##
## SAVE PROCESS INFO
echo $pid > /var/run/$script_name

if [ "$RUNNING_ARGS" != "$BACKUP_SECURITY_CODE" ]; then
        WARNING_STATUS="WARNING"
        echo "[systembckp-post][error] This backup script needs to run with backup security code as the argument for security purpose" | tee -a $REPORT_FILE
        echo "[systembckp-post][error] No backup security code or invalid backup security code is supplied. This script is terminated for this reason" | tee -a  $REPORT_FILE
        $MAIL_BIN -s "[systembckp-post][$WARNING_STATUS|N] System Backup POST Script Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
        #Remove log and lock status
        rm -f $REPORT_FILE
        rm -f "/var/run/$script_name"
        # terminate script
        exit 1
fi
# Check if the backup directory at /backup/da/admin_backups is empty or not for any potential reason
if [ ! -d "$BACKUP_DIRS" ]; then
        WARNING_STATUS="WARNING"
        echo "[systembckp-post][error] Backup directory is empty or does not exist in $SYSTEM_BACKUP_ORI_PATH to be archived" | tee -a $REPORT_FILE
else
        echo "[systembckp-post][notice] There is one or more backup directory exist in $SYSTEM_BACKUP_ORI_PATH to be archived" | tee -a $REPORT_FILE
        # Archive every single folder
        for da_backup_dir in "${BACKUP_DIRS[@]}"
        do
                echo "[systembckp-post][notice] Compressing backup folder of $da_backup_dir as tar.gz ..." | tee -a $REPORT_FILE
                tar -c --use-compress-program=pigz -f "$da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz" "$da_backup_dir" | tee -a $REPORT_FILE
                echo "[systembckp-post][done] Backup folder of $da_backup_dir has been sucessfully compressed as $da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz" | tee -a $REPORT_FILE
                ################
                echo "[systemckp-post][notice] Validating backup file $da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz ..." | tee -a $REPORT_FILE
                if pigz -cvd "$da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz" | tar -tv > /dev/null; then
                        echo "[systembckp-post][ok] Backup file validation passed for $da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz" | tee -a $REPORT_FILE
                        rm -rf $da_backup_dir
                        echo "[systembckp-post][ok] Backup folder of $da_backup_dir has been deleted" | tee -a $REPORT_FILE
                        #Change the file permissions of the complete files which located at the New full path
                        chown -R $USER_NAME:root "$da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz"
                        chmod -R 644 "$da_backup_dir$BACKUP_TIME_NOW-system_backups.tar.gz"
                        #Transfer semua files yang telah compress di root backup ke dalam path baru untuk view kat file manager
                        mv "${SYSTEM_BACKUP_ORI_PATH}"/* "${SYSTEM_BACKUP_NEW_PATH}"/
                        echo "[systembckp-post][done] Backup files from $SYSTEM_BACKUP_ORI_PATH have been transferred into $SYSTEM_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
                        echo "[systembckp-post][done] Backup file permission modified to current user at $SYSTEM_BACKUP_NEW_PATH" | tee -a $REPORT_FILE
                else
                        echo "[systembckp-post][error] File validation is not passed! Please inspect the backup archived file" |tee -a $REPORT_FILE
                        WARNING_STATUS="WARNING"
                fi
                ##############
        done
fi # END CHECK DIRECTORY BACKUP ORI


# OTHER CHECK TO DO HERE AND IF WARNING STATUS = YES SEND EMAIL
#Retransfer previous backup at the temporary file to the original folder
if find "$SYSTEM_BACKUP_TEMP_PATH" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
        echo "[systembckp-post][notice] Existing backup files found at $SYSTEM_BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
        echo "[systembckp-post][notice] Transferring existing backup to $SYSTEM_BACKUP_NEW_PATH ..." | tee -a $REPORT_FILE
        mv "${SYSTEM_BACKUP_TEMP_PATH}"/* "${SYSTEM_BACKUP_NEW_PATH}"/
        echo "[systembckp-post][done] Existing backup files have been transferred into $SYSTEM_BACKUP_NEW_PATH successfully" | tee -a $REPORT_FILE
fi
echo "[systembckp-post][done] System Backup Post Script has finished running" | tee -a $REPORT_FILE
echo "================================================================" | tee -a $REPORT_FILE
$MAIL_BIN  -s "[systembckp-post][$WARNING_STATUS] System Backup Post Script Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
rm -f $REPORT_FILE
rm -f "/var/run/$script_name"
exit 0